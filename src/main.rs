extern crate chrono;
extern crate rand;

use std::collections::HashMap;
use std::thread::sleep;
use std::time::Duration;
use std::time::SystemTime;

use chrono::Local;
use rand::Rng;
use rand::thread_rng;

use files::ReadWriteFile;

fn main() {
    let mut args = std::env::args();
    args.next();
    if let Some(arg) = args.next() {
        if arg.eq(&"-v".to_string()) || arg.eq(&"--version".to_string()) {
            println!("call_imitator {}", env!("CARGO_PKG_VERSION"));
            return;
        }
    }

    let config = match Config::new(std::env::args()) {
        Ok(config) => config,
        Err(msg) => panic!(msg)
    };
    let mut read_write_file = files::ReadWriteFileFs {};
    let mut call_imitator = DefaultCallImitator { read_write: &mut read_write_file };

    println!("Call imitation began.");
    call_imitator.imitate(config);
    println!("Call imitation ended.");
}

pub struct Config {
    pub source_file: String,
    pub cdr_folder: String,
    pub voice_folder: String,
    pub cdr_str_template: String,
    pub content_cdr_str_template: String,
    pub answer_time: u32,
    pub end_time: u32,
}

impl Config {
    pub fn new(args: std::env::Args) -> Result<Config, String> {
        let mut params = Config::parse_args_to_map(args)?;

        let source_file = Config::get_from_map_with_check(&mut params, "source_file")?;
        let cdr_folder = Config::get_from_map_with_check(&mut params, "cdr_folder")?;
        let voice_folder = Config::get_from_map_with_check(&mut params, "voice_folder")?;
        let cdr_str_template = Config::get_from_map_with_check(&mut params, "cdr_str_template")?;
        let content_cdr_str_template = Config::get_from_map_with_check(&mut params, "content_cdr_str_template")?;
        let answer_time = Config::get_from_map_or_default_value(&mut params, "answer_time", "10".to_string());
        let answer_time: u32 = match answer_time.parse() {
            Ok(seconds) => seconds,
            Err(_msg) => return Err("Can't parse arg 'answer_time'. Value must be int.".to_string())
        };

        let end_time = Config::get_end_time(&params)?;

        Ok(Config {
            source_file,
            cdr_folder,
            voice_folder,
            cdr_str_template,
            content_cdr_str_template,
            answer_time,
            end_time,
        })
    }

    fn parse_args_to_map(mut args: std::env::Args) -> Result<HashMap<String, String>, String> {
        let mut params = HashMap::new();

        args.next();

        loop {
            let next_arg = match args.next() {
                Some(arg) => arg,
                None => break
            };

            let mut split_iter = next_arg.split("=");
            let name = Config::next_from_split_with_check(&mut split_iter, &next_arg)?;
            let value = Config::next_from_split_with_check(&mut split_iter, &next_arg)?;
            params.insert(name/*.to_string()*/, value/*.to_string()*/);
        }

        Ok(params)
    }

    fn next_from_split_with_check(split: &mut std::str::Split<&str>, arg: &str) -> Result<String, String> {
        match split.next() {
            Some(value) => Ok(value.to_string()),
            None => return Err(format!(
                "Can't parse argument {}. All arguments are named and \
                        have to have form 'name=value'.", arg
            ))
        }
    }

    fn get_from_map_with_check(params: &mut HashMap<String, String>, name: &str) -> Result<String, String> {
        match params.remove(name) {
            Some(value) => Ok(value),
            None => Err(format!("Can't find param '{}'.", name))
        }
    }

    fn get_from_map_or_default_value(params: &mut HashMap<String, String>, name: &str, default_value: String) -> String {
        match params.remove(name) {
            Some(value) => value,
            None => default_value
        }
    }

    fn get_end_time(params: &HashMap<String, String>) -> Result<u32, String> {
        match params.get("end_time") {
            Some(end_time_str) => {
                let parsed_end_time: u32 = match end_time_str.parse() {
                    Ok(seconds) => seconds,
                    Err(_msg) => return Err("Can't parse arg 'end_time'. Value must be int.".to_string())
                };
                if parsed_end_time >= 1 {
                    Ok(parsed_end_time)
                } else {
                    Err("end_time should be more or equal 1.".to_string())
                }
            },
            None => Ok(0)
        }
    }
}

static NUMBER_EVENT_PLACE_HOLDER: &str = "NUMBER_EVENT";
static TIME_EVENT_PLACE_HOLDER: &str = "TIME_EVENT";
static CALL_ID_PLACE_HOLDER: &str = "CALL_ID";
static ONE_SECOND_CHUNK_SIZE: usize = 16 * 1_000;
static HEADER_LENGTH: usize = 45;

pub struct DefaultCallImitator<'a> {
    read_write: &'a mut ReadWriteFile,
}

impl<'a> DefaultCallImitator<'a> {
    fn create_and_write_to_cdr_file(&mut self, cdr_str_template: &str, event_number: &str,
                                    call_id: &str, time: &str, path_to_cdr: &str) {
        let line_vec = self.format_str_and_convert_to_bytes_with_time(cdr_str_template, event_number, call_id, time);
        if let Err(msg) = self.read_write.create_file_and_write_bytes_to_it(path_to_cdr, line_vec.as_slice()) {
            panic!("Can't write to cdr file, because {}.", msg);
        }
    }

    fn create_and_write_to_content_cdr_file(&mut self, content_cdr_str_template: &str, call_id: &str, time: &str, path_to_cdr: &str) {
        let line_bytes = self.format_content_str_and_convert_to_bytes(content_cdr_str_template, call_id, time);
        if let Err(msg) = self.read_write.create_file_and_write_bytes_to_it(path_to_cdr, line_bytes.as_slice()) {
            panic!("Can't write to content cdr file, because {}.", msg);
        }
    }

    fn write_to_cdr_file(&mut self, cdr_str_template: &str, event_number: &str, call_id: &str, path_to_cdr: &str) {
        let line_vec = self.format_str_and_convert_to_bytes(cdr_str_template, event_number, call_id);
        if let Err(msg) = self.read_write.write_bytes_to_file(path_to_cdr, line_vec.as_slice()) {
            panic!("Can't write to cdr file, because {}", msg);
        }
    }

    fn format_str_and_convert_to_bytes_with_time(&mut self, cdr_str_template: &str, event_number: &str,
                                                 call_id: &str, time: &str) -> Vec<u8> {
        let mut result = cdr_str_template.replace(NUMBER_EVENT_PLACE_HOLDER, event_number)
            .replace(TIME_EVENT_PLACE_HOLDER, &time)
            .replace(CALL_ID_PLACE_HOLDER, call_id);
        result.push_str("\n");
        result.into_bytes()
    }

    fn format_str_and_convert_to_bytes(&mut self, cdr_str_template: &str, event_number: &str, call_id: &str) -> Vec<u8> {
        let time = self.current_time_str();
        self.format_str_and_convert_to_bytes_with_time(cdr_str_template, event_number, call_id, &time)
    }

    fn format_content_str_and_convert_to_bytes(&mut self, content_cdr_str_template: &str, call_id: &str, time: &str) -> Vec<u8> {
        let mut result = content_cdr_str_template.replace(TIME_EVENT_PLACE_HOLDER, time)
            .replace(CALL_ID_PLACE_HOLDER, call_id);
	result.push_str("\n");
        result.into_bytes()
    }

    fn current_time_str(&self) -> String {
        let time_ms: u64 = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH)
            .expect("Time went backwards.")
            .as_secs() * 1_000;
        time_ms.to_string()
    }

    fn create_and_write_to_content_file(&mut self, all_bytes: &Vec<u8>, path_to_content: &str) -> usize {
        let write_bytes_count = match all_bytes.len() < HEADER_LENGTH + ONE_SECOND_CHUNK_SIZE {
            true => all_bytes.len(),
            false => HEADER_LENGTH + ONE_SECOND_CHUNK_SIZE
        };

        let bytes = self.vector_to_array(all_bytes, 0, write_bytes_count);

        if let Err(msg) = self.read_write.create_file_and_write_bytes_to_it(path_to_content, bytes.as_slice()) {
            panic!("Can't write to content file, because {}", msg)
        }

        write_bytes_count
    }

    fn write_to_content_file(&mut self, all_bytes: &Vec<u8>, path_to_content: &str, already_written_count: usize) -> usize {
        let write_bytes_count = match all_bytes.len() < already_written_count + ONE_SECOND_CHUNK_SIZE {
            true => all_bytes.len() - already_written_count,
            false => ONE_SECOND_CHUNK_SIZE
        };

        let bytes = self.vector_to_array(all_bytes, already_written_count, write_bytes_count);

        if let Err(msg) = self.read_write.write_bytes_to_file(path_to_content, bytes.as_slice()) {
            panic!("Can't write to content file, because {}", msg)
        }

        already_written_count + write_bytes_count
    }

    fn vector_to_array(&self, source: &Vec<u8>, from: usize, length: usize) -> Vec<u8> {
        let mut result: Vec<u8> = vec!();

        for i in from..(from + length) {
            let byte = match source.get(i) {
                None => panic!(
                    "Wrong index: {}. From {}, length {}, vector size {}.",
                    i, from, length, source.len()
                ),
                Some(b) => b
            };
            result.push(*byte);
        }
        result
    }

    fn construct_path_to_cdr(&self, config: &Config, prefix: &str, formatted_current_date: &str) -> String {
        let mut path_to_cdr = config.cdr_folder.clone();
        path_to_cdr.push_str(prefix);
        path_to_cdr.push_str(formatted_current_date);
        path_to_cdr.push_str("_t00.cdr");
        path_to_cdr
    }

    fn generate_call_id(&self) -> String {
        let mut result = String::new();
        result.push_str("11111");

        let mut thread_rng = thread_rng();
        for _i in 0..13 {
            let i: u8 = thread_rng.gen_range(0, 10);
            result.push_str(&i.to_string());
        }
        result
    }

    fn rename(&mut self, from: &str, to: &str) {
        if let Err(msg) = self.read_write.rename(from, to) {
            panic!("Can't rename file {}.", msg);
        }
    }
}

pub trait CallImitator {
    fn imitate(&mut self, config: Config);
}

impl<'a> CallImitator for DefaultCallImitator<'a> {
    fn imitate(&mut self, config: Config) {
        let all_bytes: Vec<u8> = match self.read_write.read_all_bytes_from_file(&config.source_file) {
            Ok(result) => result,
            Err(msg) => panic!(msg)
        };

        if all_bytes.len() < HEADER_LENGTH {
            panic!("Source file size is less than 45 bytes.")
        }

        let formatted_current_date = Local::now().format("%Y%m%d_%H%M").to_string();
        let path_to_cdr = self.construct_path_to_cdr(&config, "/mobile_calls__", &formatted_current_date);
        let path_to_content_cdr = self.construct_path_to_cdr(&config, "/content__", &formatted_current_date);

        let call_id = self.generate_call_id();
        let time = self.current_time_str();

        let mut path_to_audio_file = config.voice_folder.clone();
        path_to_audio_file.push_str("/");
        path_to_audio_file.push_str(&call_id);
        path_to_audio_file.push_str("_audio.wav");

        let mut path_to_incomplete_audio_file = path_to_audio_file.clone();
        path_to_incomplete_audio_file.push_str(".incomplete");

        self.create_and_write_to_cdr_file(&config.cdr_str_template, "65", &call_id, &time, &path_to_cdr);
        self.create_and_write_to_content_cdr_file(&config.content_cdr_str_template, &call_id, &time, &path_to_content_cdr);
        sleep(Duration::from_secs(1));
        let mut written_bytes_count = self.create_and_write_to_content_file(&all_bytes, &path_to_incomplete_audio_file);
        if written_bytes_count == all_bytes.len() {
            self.rename(&path_to_incomplete_audio_file, &path_to_audio_file);
            return;
        };

        let mut chunk_counter = 1;
        while written_bytes_count < all_bytes.len() {
            sleep(Duration::from_secs(1));
            written_bytes_count = self.write_to_content_file(&all_bytes, &path_to_incomplete_audio_file, written_bytes_count);
            if chunk_counter == config.end_time {
                println!("Finish imitation ahead of schedule because of end_time={}.", config.end_time);
                break;
            }

            chunk_counter += 1;
            if chunk_counter == config.answer_time {
                self.write_to_cdr_file(&config.cdr_str_template, "66", &call_id, &path_to_cdr);
            };
        };

        self.rename(&path_to_incomplete_audio_file, &path_to_audio_file);
        self.write_to_cdr_file(&config.cdr_str_template, "67", &call_id, &path_to_cdr);
    }
}

pub mod files {
    use std::fs::File;
    use std::fs::OpenOptions;
    use std::io;
    use std::io::Read;
    use std::io::Write;

    pub trait ReadWriteFile {
        fn read_all_bytes_from_file(&self, path: &str) -> io::Result<Vec<u8>>;
        fn create_file_and_write_bytes_to_it(&mut self, path: &str, for_write: &[u8]) -> io::Result<()>;
        fn write_bytes_to_file(&mut self, path: &str, for_write: &[u8]) -> io::Result<()>;
        fn rename(&mut self, from: &str, to: &str) -> io::Result<()>;
    }

    pub struct ReadWriteFileFs {}

    impl ReadWriteFileFs {}

    impl ReadWriteFile for ReadWriteFileFs {
        fn read_all_bytes_from_file(&self, path: &str) -> io::Result<Vec<u8>> {
            let mut file = File::open(path)?;

            let mut buffer = Vec::new();
            file.read_to_end(&mut buffer)?;
            Ok(buffer)
        }

        fn create_file_and_write_bytes_to_it(&mut self, path: &str, for_write: &[u8]) -> io::Result<()> {
            let mut file = OpenOptions::new()
                .write(true)
                .create_new(true)
                .open(path)?;

            file.write(for_write)?;
            Ok(())
        }

        fn write_bytes_to_file(&mut self, path: &str, for_write: &[u8]) -> io::Result<()> {
            let mut file = OpenOptions::new()
                .write(true)
                .append(true)
                .open(path)?;

            file.write(for_write)?;
            Ok(())
        }

        fn rename(&mut self, from: &str, to: &str) -> io::Result<()> {
            std::fs::rename(from, to)
        }
    }
}

#[cfg(test)]
mod tests {
    extern crate rand;
    extern crate regex;

    use std::collections::HashMap;
    use std::io;

    use CallImitator;
    use files::ReadWriteFile;

    use super::*;

    use self::regex::Regex;

    struct ReadWriteMockInMemory<'a> {
        bytes_should_be_read: &'a Vec<u8>,
        files_to_written_bytes: &'a mut HashMap<String, Vec<u8>>,
        renamed_files: &'a mut HashMap<String, String>,
    }

    impl<'a> ReadWriteMockInMemory<'a> {
        fn is_file_already_created(&self, path_to_file: &str) -> bool {
            self.files_to_written_bytes.contains_key(path_to_file)
        }

        fn create_file(&mut self, path_to_file: &str) {
            self.files_to_written_bytes.insert(path_to_file.to_string(), Vec::new());
        }

        fn push_to_written_bytes(&mut self, key: &str, for_write: &[u8]) {
            let all_bytes = self.files_to_written_bytes.get_mut(key).unwrap();
            for byte in for_write {
                all_bytes.push(*byte);
            }
        }

        fn put_to_renamed(&mut self, from: &str, to: &str) {
            self.renamed_files.insert(from.to_string(), to.to_string());
        }
    }

    impl<'a> ReadWriteFile for ReadWriteMockInMemory<'a> {
        fn read_all_bytes_from_file(&self, _path: &str) -> io::Result<Vec<u8>> {
            Ok(self.bytes_should_be_read.clone())
        }

        fn create_file_and_write_bytes_to_it(&mut self, path: &str, for_write: &[u8]) -> io::Result<()> {
            assert_eq!(false, self.is_file_already_created(path));
            self.create_file(path);
            self.push_to_written_bytes(path, for_write);
            Ok(())
        }

        fn write_bytes_to_file(&mut self, path: &str, for_write: &[u8]) -> io::Result<()> {
            assert_eq!(true, self.is_file_already_created(path));
            self.push_to_written_bytes(path, for_write);
            Ok(())
        }

        fn rename(&mut self, from: &str, to: &str) -> io::Result<()> {
            self.put_to_renamed(from, to);
            Ok(())
        }
    }

    #[test]
    fn should_properly_imitate_call_with_source_bytes_not_multiple_to_chunk_size() {
        should_properly_imitate_call(HEADER_LENGTH + ONE_SECOND_CHUNK_SIZE * 2 + 345)
    }

    #[test]
    fn should_properly_imitate_call_with_source_bytes_multiple_to_chunk_size() {
        should_properly_imitate_call(HEADER_LENGTH + ONE_SECOND_CHUNK_SIZE * 3)
    }

    fn should_properly_imitate_call(source_file_size: usize) {
        let bytes_for_read = random_bytes(source_file_size);
        let mut files_to_bytes: HashMap<String, Vec<u8>> = HashMap::new();
        let mut renamed_files: HashMap<String, String> = HashMap::new();
        perform_imitation(&bytes_for_read, &mut files_to_bytes, &mut renamed_files);

        let size = files_to_bytes.len();
        assert_eq!(3, size);

        let audio_file_regex = Regex::new(r"voice_folder/(\d{18})_audio.wav.incomplete").unwrap();
        let cdr_line_regex = Regex::new(r"prefix_field;(\d{13});some_field;(\d{18});some_field;(\d{2});suffix_field;").unwrap();
        let content_cdr_line_regex = Regex::new(r"prefix_field;(\d{13});(\d{18});(\d{18})_audio###audio").unwrap();
        let mut call_id = String::new();

        for (key, value) in files_to_bytes.iter() {
            if audio_file_regex.is_match(key) {
                let cap = audio_file_regex.captures(key).unwrap();
                if call_id.is_empty() {
                    call_id.push_str(&cap[1]);
                } else {
                    assert_eq!(&call_id, &cap[1]);
                }
                assert_eq!(bytes_for_read, *value);
                continue;
            };

            if key.starts_with("cdr_folder") {
                if key.starts_with("cdr_folder/mobile_calls__") {
                    assert_eq!(true, key.ends_with("_t00.cdr"));
                    let cdr_str = bytes_to_str(value.clone());
                    let mut lines = cdr_str.lines();

                    let first_line = lines.next().unwrap();
                    assert_eq!(true, cdr_line_regex.is_match(first_line));
                    let cap = cdr_line_regex.captures(first_line).unwrap();
                    if call_id.is_empty() {
                        call_id.push_str(&cap[2]);
                    } else {
                        assert_eq!(&call_id, &cap[2]);
                    }
                    assert_eq!("65", &cap[3]);

                    let second_line = lines.next().unwrap();
                    assert_eq!(true, cdr_line_regex.is_match(second_line));
                    let cap = cdr_line_regex.captures(second_line).unwrap();
                    assert_eq!("66", &cap[3]);
                    assert_eq!(&call_id, &cap[2]);

                    let third_line = lines.next().unwrap();
                    assert_eq!(true, cdr_line_regex.is_match(third_line));
                    let cap = cdr_line_regex.captures(third_line).unwrap();
                    assert_eq!("67", &cap[3]);
                    assert_eq!(&call_id, &cap[2]);

                    continue
                }
                if key.starts_with("cdr_folder/content__") {
                    assert_eq!(true, key.ends_with("_t00.cdr"));
                    let value_clone = value.clone();
                    let str_content = bytes_to_str(value_clone);

                    let single_line = str_content.lines().next().unwrap();
                    assert_eq!(true, content_cdr_line_regex.is_match(single_line));
                    let cap = content_cdr_line_regex.captures(single_line).unwrap();
                    if call_id.is_empty() {
                        call_id.push_str(&cap[2]);
                    } else {
                        assert_eq!(&call_id, &cap[2]);
                    };
                    assert_eq!(&call_id, &cap[3]);
                    continue
                }
            }
            panic!("Unknown key {}.", key);
        }

        assert_eq!(1, renamed_files.len());
        let path_to_audio_file = path_to_audio_file(&call_id);
        let path_to_incomplete = path_to_incomplete_audio_file(&path_to_audio_file);
        assert_eq!(path_to_audio_file, *renamed_files.get(&path_to_incomplete).unwrap());
    }

    fn path_to_audio_file(call_id: &str) -> String {
        let mut result = "voice_folder/".to_string();
        result.push_str(call_id);
        result.push_str("_audio.wav");
        result
    }

    fn path_to_incomplete_audio_file(path_to_file: &str) -> String {
        let mut result = path_to_file.to_string();
        result.push_str(".incomplete");
        result
    }

    fn perform_imitation(bytes_for_read: &Vec<u8>, files_to_bytes: &mut HashMap<String, Vec<u8>>, renamed: &mut HashMap<String, String>) {
        let mut read_write_mock = ReadWriteMockInMemory {
            bytes_should_be_read: bytes_for_read,
            files_to_written_bytes: files_to_bytes,
            renamed_files: renamed,
        };
        let mut imitator = DefaultCallImitator { read_write: &mut read_write_mock };

        let config = Config {
            source_file: "source_file".to_string(),
            cdr_folder: "cdr_folder".to_string(),
            voice_folder: "voice_folder".to_string(),
            cdr_str_template: cdr_template(),
            content_cdr_str_template: content_cdr_template(),
            answer_time: 2,
            end_time: 0
        };

        imitator.imitate(config);
    }

    fn cdr_template() -> String {
        let mut template = String::new();
        template.push_str("prefix_field;");
        template.push_str(TIME_EVENT_PLACE_HOLDER);
        template.push_str(";some_field;");
        template.push_str(CALL_ID_PLACE_HOLDER);
        template.push_str(";some_field;");
        template.push_str(NUMBER_EVENT_PLACE_HOLDER);
        template.push_str(";suffix_field;");
        template
    }

    fn content_cdr_template() -> String {
        let mut content_template = String::new();
        content_template.push_str("prefix_field;");
        content_template.push_str(TIME_EVENT_PLACE_HOLDER);
        content_template.push_str(";");
        content_template.push_str(CALL_ID_PLACE_HOLDER);
        content_template.push_str(";");
        content_template.push_str(CALL_ID_PLACE_HOLDER);
        content_template.push_str("_audio###audio");
        content_template
    }

    fn random_bytes(bytes_count: usize) -> Vec<u8> {
        let mut result: Vec<u8> = Vec::new();
        for _i in 0..bytes_count {
            result.push(rand::random())
        }
        result
    }

    fn bytes_to_str(bytes: Vec<u8>) -> String {
        let str_from_bytes = match String::from_utf8(bytes) {
            Ok(str) => str,
            Err(msg) => panic!("{}", msg)
        };
        str_from_bytes
    }
}
